﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keyleft : MonoBehaviour {


    private Animator animator;
    private  Animation Animation;
    public AnimationClip[] clips;
    // Use this for initialization
    void Start () {
        animator = this.gameObject.GetComponent<Animator>();
        Animation = this.gameObject.GetComponent<Animation>();
    }
	
	// Update is called once per frame
	void Update () {
        if(Input .GetKeyDown (KeyCode .A)) {

            Animation.clip = clips[0];
            Animation.Play();
        }
    }
}
