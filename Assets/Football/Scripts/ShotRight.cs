﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotRight : MonoBehaviour {

   
  
    private  ScoreManger1 ScoreManger;
    public GameObject target;//keeperrighthand or left


    shooting shot;


    void Start () {

        ScoreManger = GameObject.FindGameObjectWithTag("scoremanger").GetComponent<ScoreManger1>();
    }
	
	
    public void ShootBallRight()
    {

        if (GameObject.FindGameObjectWithTag("ball").GetComponent<shooting>().isRightEmail)
        {
            if (GameObject.FindGameObjectWithTag("ball")!=null )
            {

                GameObject.FindGameObjectWithTag("ball").GetComponent<shooting>().ShootBall(target, GameObject.FindGameObjectWithTag("ball"));
                ScoreManger.scoreInc();
            }
          
        }
    }

   
}
