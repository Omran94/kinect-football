﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanstion:MonoBehaviour  {

    public GameObject mail;
    public GameObject ball;
    public GameObject ballPos;
    [HideInInspector ]
    public GameObject instMail;
    [HideInInspector]
    public GameObject instball;
    public bool isBallInst ;
    public bool isMailInst;
    float time = 1;
    private MoveEmails moveEmails;
    public GameObject player;

    bool check=true ;
    int counter = 0;

    public GameObject GameEndPanel;


    void Start() {
        if(moveEmails == null)
            return;
        moveEmails = GameObject.FindGameObjectWithTag("emailpanel").GetComponent<MoveEmails>();
        
    }

    // Update is called once per frame
    void Update() {

       

        SpawnEmails();



        if(player.GetComponent<AnimationComplete>().IsCompleteAnim) {
                Destroy(instMail);
                Destroy(instball);
                check = true;
                time -= Time.deltaTime;
           
                if(time <= 0) {
                    if(check) {
                    counter++;
                    instball = Instantiate(ball,ballPos.transform.position,Quaternion.identity);
                        instMail = Instantiate(mail);
                   
                    check = false;
                        player.GetComponent<AnimationComplete>().IsCompleteAnim = false;

                   

                }
                    time = 1;
                }
            
        }
     
        if(counter ==3) {
           
            GameEndPanel.SetActive(true);
            Time.timeScale = 0;
        }
       
    }

   public  void SpawnEmails() {
        if(GameObject.FindGameObjectWithTag("emailpanel") != null) {

            moveEmails = GameObject.FindGameObjectWithTag("emailpanel").GetComponent<MoveEmails>();
            if(moveEmails.isBeginPlay) {
                
                if(isBallInst) {
                 
                    instball = Instantiate(ball,ballPos.transform.position,Quaternion.identity);
                    
                    isBallInst = false;
                }
               
            }
        }




        if(isMailInst) {
            instMail = Instantiate(mail);
            isMailInst = false;
        }



    }



}
