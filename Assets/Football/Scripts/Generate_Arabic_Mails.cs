﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text.RegularExpressions;

public class Generate_Arabic_Mails:MonoBehaviour {
    private Text email;
    public TextAsset wordFile;
    private List<string> lineList = new List<string>();
    static Regex ValidEmailRegex = CreateValidEmailRegex();
    bool check = true;
    bool isValid;

    void Start() {
       
        email = this.GetComponent<Text>();
        ReadWordList();
     
        //Debug.Log("Random line from list: " + GetRandomLine());
      
    }


    private void Update() {
        if(check ) {
           

            email.text = GetRandomLine();
            Debug.Log(EmailIsValid(email.text));
            check = false;

        }

    }

    public void ReadWordList() {

        if(wordFile) {
            string line;

            StringReader textStream = new StringReader(wordFile.text);

            while((line = textStream.ReadLine()) != null ) {
            
                lineList.Add(line);
            }

        //    textStream.Close();
        }
    }

    public string GetRandomLine() {

        return lineList[Random.Range(0,lineList.Count)];
    }

    private static Regex CreateValidEmailRegex() {
        string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
              + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
              + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

        return new Regex(validEmailPattern,RegexOptions.IgnoreCase);
    }

    public bool EmailIsValid(string emailAddress) {

        if(emailAddress=="lailaahmed18@outlook.com") {
            isValid = true;
        }
        return isValid;
    }
}