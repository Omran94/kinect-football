﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shooting : MonoBehaviour
{

    private GameObject target1;
    private GameObject target2;
    private GameObject target3;

    public float power = 10;
    private ChooseEmail chooseEmail;
    bool flag = true;
    bool flag2 = true;
    [HideInInspector]
    public bool isRightChoice = false;
    [HideInInspector]
    public bool isShooted = false;
    private MoveEmails moveEmails;
    private Animator anim;
    private GenerateEmailImages generate;
    private GameObject player;

    private Collider PlayerHandR;
    private Collider PlayerHandM;
    private Collider PlayerHandL;

    public bool isRightEmail = false;


    int counter = 0;
    float time = 2;

    Vector3 startPosition;
  

    private void Start()
    {
        time = 2;
        target1 = GameObject.FindGameObjectWithTag("t1");
        target2 = GameObject.FindGameObjectWithTag("t2");
        target3 = GameObject.FindGameObjectWithTag("t3");
        moveEmails = GameObject.FindGameObjectWithTag("emailpanel").GetComponent<MoveEmails>();
        generate = GameObject.FindGameObjectWithTag("emailpanel").GetComponent<GenerateEmailImages>();
        player = GameObject.FindGameObjectWithTag("Player");
        anim = player.GetComponent<Animator>();


        startPosition = player.transform.position;

        PlayerHandR = GameObject.FindGameObjectWithTag("Rhand").GetComponent<BoxCollider>();
        PlayerHandL = GameObject.FindGameObjectWithTag("Lhand").GetComponent<BoxCollider>();
        PlayerHandM = GameObject.FindGameObjectWithTag("Mhand").GetComponent<BoxCollider>();

      

    }

    // Update is called once per frame
    private void Update()
    {




        #region kinect
     

            if (KinectManager.instance.IsKneeRight || Input.GetKeyDown(KeyCode.D))
            {


                for (int i = 0; i < generate.falseEmails.Length; i++)
                {


                    if (GameObject.Find("B3").GetComponent<Image>().sprite == generate.falseEmails[i])
                    {

                      
                        isRightEmail = true;
                        PlayerHandR.isTrigger = false;
                        anim.SetInteger("direction ", 3);



                    }
                }
                for (int i = 0; i < generate.trueEmails.Length; i++)
                {

                    if (GameObject.Find("B3").GetComponent<Image>().sprite == generate.trueEmails[i])
                    {

                        PlayerHandR.isTrigger = true;

                        anim.SetInteger("direction ", 1);

                        ShootBall(target3, this.gameObject);

                    }
                }



                KinectManager.instance.IsKneeRight = false;
            }

            if (KinectManager.instance.IsKneeLeft || Input.GetKeyDown(KeyCode.A))
            {


                for (int i = 0; i < generate.falseEmails.Length; i++)
                {

                    if (GameObject.Find("B1").GetComponent<Image>().sprite == generate.falseEmails[i])
                    {

                        isRightEmail = true;
                        PlayerHandL.isTrigger = false;
                        anim.SetInteger("direction ", 1);



                    }
                }
                for (int i = 0; i < generate.trueEmails.Length; i++)
                {

                    if (GameObject.Find("B1").GetComponent<Image>().sprite == generate.trueEmails[i])
                    {
                        PlayerHandL.isTrigger = true;
                        anim.SetInteger("direction ", 2);

                        ShootBall(target1, this.gameObject);
                    }

                }





                KinectManager.instance.IsKneeLeft = false;
            }

            if (KinectManager.instance.IsClosed || Input.GetKeyDown(KeyCode.W))
            {

                for (int i = 0; i < generate.falseEmails.Length; i++)
                {

                    if (GameObject.Find("B2").GetComponent<Image>().sprite == generate.falseEmails[i])
                    {
                        isRightEmail = true;
                        PlayerHandM.isTrigger = false;
                        anim.SetInteger("direction ", 2);



                    }
                }
                for (int i = 0; i < generate.trueEmails.Length; i++)
                {
                    if (GameObject.Find("B2").GetComponent<Image>().sprite == generate.trueEmails[i])
                    {
                        PlayerHandM.isTrigger = true;
                        anim.SetInteger("direction ", 3);

                        ShootBall(target2, this.gameObject);
                    }

                }

                KinectManager.instance.IsClosed = false;
            }
       
        #endregion

    }

    public void ShootBall(GameObject target, GameObject ball)
    {
        isShooted = true;
        if (flag == true)
        {

            Vector3 dirc = target.transform.position - ball.transform.position;

            this.GetComponent<Rigidbody>().AddForce(dirc.x * power, dirc.y * power + 20, dirc.z * power, ForceMode.Force);

            flag = false;
        }





    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "goal")
        {

            time -= Time.deltaTime;
            if (time <= 0)
            {
                Debug.Log("laila");
                Destroy(this.gameObject);
            }


        }
    }


}