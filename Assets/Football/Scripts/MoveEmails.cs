﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveEmails:MonoBehaviour {
    RectTransform tran;
    public GameObject mailTarget1;
    public GameObject mailTarget2;
    public float speed = 10;
    public float time ;
    bool flag = true;
    public bool isBeginPlay = false;
   


    // Use this for initialization
    void Start() {
        tran = GetComponent<RectTransform>();
      
    }

    // Update is called once per frame
    void Update() {



        time -= Time.deltaTime;
       
        if(time < 100 && time > 0) {
       
            MoveTOT1();
            isBeginPlay = false ;
        }

        if(time <= 0 ) {

            MoveTOT2();
           
     
        }
   
    }


    private void MoveTOT1() {

        // Debug.Log("1");

        transform.position = Vector3.MoveTowards(transform.position,new Vector3(mailTarget1.transform.position.x,mailTarget1.transform.position.y,mailTarget1.transform.position.z),speed * Time.deltaTime);

       


    }

    private void MoveTOT2() {
        transform.position = Vector3.MoveTowards(transform.position,new Vector3(mailTarget2.transform.position.x,mailTarget2.transform.position.y,mailTarget2.transform.position.z),speed * Time.deltaTime);


       
        this.gameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2 (310,200);
        this.gameObject.GetComponent<GridLayoutGroup>().spacing  = new Vector2 (3,0);

        if (this .transform .position == mailTarget2 .transform .position ) {
            
            isBeginPlay = true;
        }
    }

}
