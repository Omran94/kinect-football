﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;


public class GenerateEmails:MonoBehaviour {
    public Text test;
    bool check = true;
    static Regex ValidEmailRegex = CreateValidEmailRegex();
    int indexRandomChar1;
    int indexRandomChar2;
    int indexRandomChar3;
    int indexRandomChar4;
    int indexRandomChar5;
    int indexRandomChar6;
    int indexRandomChar7;



    void Start() {


    }


    void Update() {

        if(check) {

            test.text = Randomness();
           Debug.Log(EmailIsValid(test.text));

            check = false;


        }


    }

    public string Randomness() {


        string[] names = { "laila","Ahmed","ali","Mohamed","Hadeer","hamdy","ibrahim","nour","hamed","abdallah","zien","ziad","hussien","essam","asmaa","enas","ashgan","amin","youssef","bakr","Rana",};
        string[] numbers = { "1","2","3","4","5","6","7","8","9","10" };
        string[] symbols = { "@","#","$","%","^","&","*","!","/","|","/","<",">","?" };
        string[] requird = { "@" };
        string[] emailSlogan = { "outlook","gmail","yahoo","live" };
        string[] requird2 = { "." };
        string[] com = { "c","co","com" };

        indexRandomChar1 = UnityEngine.Random.Range(0,names.Length);
        indexRandomChar2 = UnityEngine.Random.Range(0,numbers.Length);
        indexRandomChar3 = UnityEngine.Random.Range(0,symbols.Length);
        indexRandomChar4 = UnityEngine.Random.Range(0,requird.Length);
        indexRandomChar5 = UnityEngine.Random.Range(0,requird2.Length);
        indexRandomChar6 = UnityEngine.Random.Range(0,emailSlogan.Length);
        indexRandomChar7 = UnityEngine.Random.Range(0,com.Length);

        return names[indexRandomChar1] + numbers[indexRandomChar2] + symbols[indexRandomChar3] + requird[indexRandomChar4] + emailSlogan[indexRandomChar6] + requird2[indexRandomChar4] + com[indexRandomChar7];
    }



    private static Regex CreateValidEmailRegex() {
        string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
            + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
            + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

        return new Regex(validEmailPattern,RegexOptions.IgnoreCase);
    }

    public bool EmailIsValid(string emailAddress) {
        bool isValid = ValidEmailRegex.IsMatch(emailAddress);

        return isValid;
    }
}
