﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseEmail:MonoBehaviour {

    private Button button;
    public GameObject ball;
    private float power = 100;
    public bool check = true;
  


    // Use this for initialization
    void Start() {
        button = GetComponent<Button>();
       


        button.onClick.AddListener(() =>
        {
            check = false;

          


            ShootBall();


        });
    }

    // Update is called once per frame
    void Update() {




    }
    void ShootBall() {

        Vector3 dirc = this.transform.position - ball.transform.position;

        ball.GetComponent<Rigidbody>().AddForce(dirc.x * power,dirc.y * power + 20,dirc.z * power,ForceMode.Force);



    }

}
