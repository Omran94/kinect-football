﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SendToGoogle : MonoBehaviour {

    public GameObject userName;
    public GameObject email;

    private string Name;
    private string Email;

    [SerializeField]
    private string Base_URL = "https://docs.google.com/forms/d/e/1FAIpQLSeQ-4iGWcgt90NHCtGE5oMI_dyhit9TVZ19UltizS-ZJTB1uA/formResponse";


    public void send() {
        Name = userName.GetComponent<InputField>().text;
        Email = email.GetComponent<InputField>().text;

        StartCoroutine(post(Name ,Email ));
        SceneManager.LoadScene(0);
    }

      IEnumerator post(string name,string email) {
        WWWForm form = new WWWForm ();
        form.AddField("entry.1138026965",name );
        form.AddField("entry.734294985",email );

        byte[] rawData = form.data;
        WWW www = new WWW(Base_URL,rawData);
        yield return www;
      

    }
}
