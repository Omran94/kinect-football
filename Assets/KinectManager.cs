﻿using UnityEngine;
using UnityEngine.UI;

using Windows.Kinect;

using System.Linq;
using System;

public class KinectManager:MonoBehaviour {
    private KinectSensor _sensor;
    private BodyFrameReader _bodyFrameReader;
    private Body[] _bodies = null;




    public bool IsAvailable;

    public bool IsFire = false;
    public bool IsClosed = false;
    public bool IsKneeRight = false;
    public bool IsKneeLeft = false;
    public double lastFire = 0;



    public static KinectManager instance = null;

    private GameObject playerFootRight;
    private GameObject playerFootLeft;
    private GameObject ball;
    private MoveEmails moveEmails;


    public Body[] GetBodies() {
        return _bodies;
    }

    void Awake() {
        if(instance == null) {
            instance = this;
        }
        else if(instance != this)
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start() {
      
        _sensor = KinectSensor.GetDefault();

        if(_sensor != null) {
            IsAvailable = _sensor.IsAvailable;



            _bodyFrameReader = _sensor.BodyFrameSource.OpenReader();

            if(!_sensor.IsOpen) {
                _sensor.Open();
            }

            _bodies = new Body[_sensor.BodyFrameSource.BodyCount];
        }



      



    }

    // Update is called once per frame
    void Update() {


        try {

            moveEmails = GameObject.FindGameObjectWithTag("emailpanel").GetComponent<MoveEmails>();

            ball = GameObject.FindGameObjectWithTag("ball");
         
       


        }
        catch(NullReferenceException e) { // --> You may use this type of exception class

        }



        //   if(moveEmails != null) {

        //    moveEmails = GameObject.FindGameObjectWithTag("emailpanel").GetComponent<MoveEmails>();
        //    Debug.Log("laila");
        //}


        //if(ball != null) {

        //    ball = GameObject.FindGameObjectWithTag("ball");
        //}

        //if(playerFootRight != null) {
        //    playerFootRight = GameObject.Find("KneeRight").gameObject;
        //}


        //if(playerFootLeft != null) {
        //    playerFootLeft = GameObject.Find("KneeLeft").gameObject;
        //}





        IsAvailable = _sensor.IsAvailable;


        if(_bodyFrameReader != null) {
          
            var frame = _bodyFrameReader.AcquireLatestFrame();
            //if(playerFootRight != null) {
            //    playerFootRight = GameObject.Find("KneeRight").gameObject;
            //}

            //if(playerFootLeft != null) {
            //    playerFootLeft = GameObject.Find("KneeLeft").gameObject;
            //}





            if(frame != null) {
                frame.GetAndRefreshBodyData(_bodies);
              
                foreach(var body in _bodies.Where(b => b.IsTracked)) {
                    IsAvailable = true;


                   

                    float distanceRL = Mathf.Abs(playerFootRight.transform.position.z - playerFootLeft.transform.position.z);
                    float distanceLR = Mathf.Abs(playerFootLeft.transform.position.z - playerFootRight.transform.position.z);

                    if(moveEmails.isBeginPlay) {

                        if(distanceRL > 0.1f) {

                          

                            double now = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalMilliseconds;


                            if(now - lastFire > 10000) {
                                lastFire = now;
                                // Debug.Log(lastFire);
                                IsKneeRight = true;

                            }



                        }



                        if(distanceLR > 1) {


                            double now = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalMilliseconds;


                            if(now - lastFire > 10000) {
                                lastFire = now;
                                // Debug.Log(lastFire);
                                IsKneeLeft = true;

                            }




                        }


                        if((body.HandRightConfidence == TrackingConfidence.High) && (body.HandRightState == HandState.Closed || body.HandLeftState == HandState.Closed)) {


                            double now = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalMilliseconds;


                            if(now - lastFire > 10000) {
                                lastFire = now;
                                // Debug.Log(lastFire);
                                IsClosed = true;

                            }


                        }





                        //    if(body.HandRightConfidence == TrackingConfidence.High && (body.HandRightState == HandState.Lasso)) {

                        //        double now = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalMilliseconds;


                        //        if(now - lastFire > 1000) {
                        //            lastFire = now;
                        //            // Debug.Log(lastFire);
                        //            IsFire = true;

                        //        }


                        //    }

                    }

                    frame.Dispose();
                    frame = null;
                }
            }
        }
        

    }

    void OnApplicationQuit() {
        if(_bodyFrameReader != null) {
            _bodyFrameReader.IsPaused = true;
            _bodyFrameReader.Dispose();
            _bodyFrameReader = null;
        }

        if(_sensor != null) {
            if(_sensor.IsOpen) {
                _sensor.Close();
            }

            _sensor = null;
        }
    }
}





