﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coll : MonoBehaviour
{

    public Transform SplatterMellon;
    public Transform SplatterApple;
    public Transform SplatterKiwi;
    public Transform MelonSlice;
    public Transform AppleSlice;
    public Transform KiwiSlice;
    public Transform clone;
    public AudioClip splatSound;

    public UnityEngine.UI.Text score;
    Transform slice1;
    Transform slice2;
    Transform splat;

    private void OnTriggerEnter(Collider col)
    {
        GameObject SliceHit;
        SliceHit = col.gameObject;
        if (SliceHit.tag == "Fruit" && SliceHit.GetComponent<Passwords>().CheckPassword() == true)
        {


            var scr = int.Parse(score.text) + 1;
            score.text = scr.ToString();
            //     PlayerPrefs.SetString("score",score.text);
        }
        //if(SliceHit.tag == "Fruit" && SliceHit.GetComponent<PasswordScript>().isStrong(SliceHit.transform.GetChild(0).GetComponent<TextMesh>().text) == false) {
        //    var scr = int.Parse(score.text);
        //    if(scr > 0) {
        //        scr = int.Parse(score.text) - 1;
        //        score.text = scr.ToString();
        //        PlayerPrefs.SetString("score",score.text);
        //    }

        //   }
        if (SliceHit.tag == "Fruit")
        {




            this.GetComponent<AudioSource>().PlayOneShot(splatSound);
            //get the speed and rotation and than destroy the fruit
            var VelocityF = SliceHit.transform.GetComponent<Rigidbody>().velocity;
            var AngularVelocityF = SliceHit.transform.GetComponent<Rigidbody>().angularVelocity;

            if (SliceHit.name == "ApplePass(Clone)")
            {
                slice1 = Instantiate(AppleSlice, SliceHit.transform.position, Quaternion.identity);
                slice2 = Instantiate(AppleSlice, SliceHit.transform.position, Quaternion.Euler(0, 180, 0));
                splat = Instantiate(SplatterApple, SliceHit.transform.position + new Vector3(0, 0, 1), Quaternion.Euler(0, 180, Random.Range(0, 360)));
            }

            if (SliceHit.name == "MelonPass(Clone)")
            {

                slice1 = Instantiate(MelonSlice, SliceHit.transform.position, Quaternion.identity);
                slice2 = Instantiate(MelonSlice, SliceHit.transform.position, Quaternion.Euler(0, 180, 0));
                splat = Instantiate(SplatterMellon, SliceHit.transform.position + new Vector3(0, 0, 1), Quaternion.Euler(0, 180, Random.Range(0, 360)));
            }

            if (SliceHit.name == "KiwiPass(Clone)")
            {
                slice1 = Instantiate(KiwiSlice, SliceHit.transform.position, Quaternion.identity);
                slice2 = Instantiate(KiwiSlice, SliceHit.transform.position, Quaternion.Euler(0, 180, 0));
                splat = Instantiate(SplatterKiwi, SliceHit.transform.position + new Vector3(0, 0, 1), Quaternion.Euler(0, 180, Random.Range(0, 360)));
            }



            Destroy(slice2.gameObject, 3);
            Destroy(slice1.gameObject, 3);
            Destroy(splat.gameObject, 3);
            Destroy(SliceHit);

        }

    }
    private void Update()
    {
        PlayerPrefs.SetString("score", score.text);
    }
}
