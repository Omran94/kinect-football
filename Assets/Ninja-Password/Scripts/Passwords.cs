﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Passwords:MonoBehaviour {
    public static int offset;
    Dictionary<int,string> passwordS = new Dictionary<int,string>();
    Dictionary<int,string> passwordW = new Dictionary<int,string>();
    [HideInInspector]
    public TextMesh passwordText;
    // Use this for initialization
    void Start() {
        //   SpawnPassword();

        System.Random randomizer = new System.Random();
        int funcToChoose = randomizer.Next(2);

        switch(funcToChoose) {
            case 0:
                // Fire the first function
                SpawnStrongPassword();
                break;
            case 1:
                // Fire the second function
                SpawnWeakPassword();
                break;

        }

    }

  
    void SpawnStrongPassword() {
        GameObject child = this.transform.GetChild(0).gameObject;
        passwordText = child.gameObject.GetComponent<TextMesh>();
        passwordText.text = RandomStrongPassword();

    }


    void SpawnWeakPassword() {
        GameObject child = this.transform.GetChild(0).gameObject;
        passwordText = child.gameObject.GetComponent<TextMesh>();
        passwordText.text = RandomWeakPassword ();

    }



    private string RandomStrongPassword() {
        offset = Random.Range(10,passwordS.Keys.Count);

        if(!passwordS.ContainsKey(offset)) {
            //strong
            passwordS.Add(1,"F@Ntast!C");
            passwordS.Add(2,"P@ssw0rD");
            passwordS.Add(3,"W34th3R20d4y");
            passwordS.Add(4,"D3cp@102");
            passwordS.Add(5,"A$T0nM4R10");
            passwordS.Add(6,"Turl1ngt0n");
            passwordS.Add(7,"TmB1w2R!");
            passwordS.Add(8,"!NsP1R0N00");
            passwordS.Add(9,"H!5HTr4Ff!C");
            passwordS.Add(10,"C0mP1!xX");
           



        }

     //  Debug.Log("strong");

        return passwordS[offset];



    }

    private string RandomWeakPassword() {
        offset = Random.Range(32,passwordW.Keys.Count);

        if(!passwordW.ContainsKey(offset)) {
            //weak
            passwordW.Add(1,"1234");
            passwordW.Add(2,"1111");
            passwordW.Add(3,"5555");
            passwordW.Add(4,"2222");
            passwordW.Add(5,"2017");
            passwordW.Add(6," 2018");
            passwordW.Add(7,"8888");
            passwordW.Add(8,"123456");
            passwordW.Add(9,"111111");
            passwordW.Add(10,"1010");
            passwordW.Add(11,"2020");
            passwordW.Add(12,"88888");
            passwordW.Add(13,"555555");
            passwordW.Add(14,"zzzz");
            passwordW.Add(15,"abc123");
            passwordW.Add(16,"password");
            passwordW.Add(17,"football");
            passwordW.Add(18,"hello");
            passwordW.Add(19,"12345");
            passwordW.Add(20,"2020");
            passwordW.Add(21,"superman");
            passwordW.Add(23,"michel");
            passwordW.Add(24,"111111");
            passwordW.Add(25,"4444");
            passwordW.Add(26,"basketball");
            passwordW.Add(27,"love");
            passwordW.Add(28,"happy");
            passwordW.Add(29,"1212");
            passwordW.Add(30,"22222");
            passwordW.Add(31,"master");
            passwordW.Add(32,"admin");

        }
      // Debug.Log("weak");

        return passwordW[offset];
    }

    public bool CheckPassword() {
        
        if(passwordS.ContainsKey(offset) ) {
           
            return true;
        }

        return false;


    }
}