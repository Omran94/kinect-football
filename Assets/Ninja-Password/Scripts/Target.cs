﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(-6f,6f),Random.Range(-6f,6f),Random.Range(-6f,6f));
        float x = Random.Range(-0.1f,0.1f);
        float y = Random.Range(1.2f,1.4f);

        GetComponent<Rigidbody>().AddForce(new Vector3(x ,y,0f));

		//Destroy the fruit so we dont fill up our memory
		if (name.Contains("Clone")) {
			Destroy (gameObject, 10);
		}

      
    }
}
