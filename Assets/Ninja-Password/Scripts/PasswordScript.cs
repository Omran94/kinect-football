﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasswordScript : MonoBehaviour {

   
	
	private float myRadius = 5;
	private float maxSceneX = 15;
	private float minSceneX = -15;
    [HideInInspector ]
	public  TextMesh text;
    [HideInInspector ]
	public  string password = "";
	private string[] letters = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"1","2","3","4","5", "6", "7", "8", "9", "0",
		"@", "#", "$", "%", "^", "&", "*", "!", "/", "|", "/","<",">","?" };
	
	void Start () {
		GameObject child = this.transform.GetChild (0).gameObject;
		text = child.gameObject.GetComponent<TextMesh> ();
		password = randomPassword ();
		text.text = password;
       
	}

	string randomPassword() {
		string pass = "";
		int count = Random.Range (8, 10);
		
        for (int i = 0; i < count ; i++) {
			int index = Random.Range (0, letters.Length);
			pass += letters [index];
		}
		return pass;
	}
    // Update is called once per frame
    void Update() {

    }
    public bool isStrong(string password) {
        if(password.Length < 8) {
            return false;
        }
        char[] lower = new char[] { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' };
        char[] upper = new char[] { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' };
        char[] digit = new char[] { '1','2','3','4','5','6','7','8','9','0',};
        char[] specialCharacters = new char[] { '@','#','$','%','^','&','*','!','/','|','/','<','>','?' };
        if(password.IndexOfAny(lower) >= 0 && //Lower case 
            password.IndexOfAny(upper) >= 0 &&
            password.IndexOfAny(digit) >= 0 &&
            password.IndexOfAny(specialCharacters) >= 0) {
            Debug.Log("strong");
            return true;

        }
        return false;
    }


}
