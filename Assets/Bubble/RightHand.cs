﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHand:MonoBehaviour {

    private GameObject requiredJoint;
    public ScoreManger scoreManager;
    public string JointName;
    private SpawnBubbleURLS spawnBubble;
    public GameObject SpawnBubble;
    public ParticleSystem Burst1;
   

    // Use this for initialization
    void Start() {
        spawnBubble = SpawnBubble.GetComponent<SpawnBubbleURLS>();
        Burst1.Stop();
    }

    // Update is called once per frame
    void Update() {
        if(requiredJoint == null) {
            requiredJoint = GameObject.Find(JointName);

        }

        else {
            gameObject.transform.position = new Vector3(requiredJoint.transform.position.x,requiredJoint.transform.position.y,transform.position.z);
        }
    }


    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "bubble") {

            string url = other.gameObject.transform.GetChild(0).GetComponent<TextMesh>().text;

            if(spawnBubble.CheckURL()) {

                scoreManager.scoreInc();

            }
            else {

                scoreManager.livesDec();

            }




            Vector3 pos = new Vector3(other.transform.position.x,other.transform.position.y,other.transform.position.z);


            Destroy(other.gameObject);
            Burst1.transform.position = pos;
          


            Burst1.Play();
           

        }
        else {
            Burst1.Stop();
           
        }
    }


}
