﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManger : MonoBehaviour {


    public GameObject ScoreObj;
	private UnityEngine.UI.Text ScoreText;

    private int score = 0;
	private int lives = 5;
	public GameObject gameEndPanel;
	public bool run = true;
	public float speed = 4f;
	public int spawnCount = 1;
	public int oldCount = 0;
	public float wait = 10f;

    public Text ScoreEndText;

    public Image LiveImage;
    public Sprite[] hearts;


    public UnityEngine.UI.Text GameTimer;

    // Use this for initialization
    void Start () {
        CountDown();
        ScoreText = ScoreObj.GetComponent<UnityEngine.UI.Text> ();

	}
    void CountDown() {
        int curTime = int.Parse(GameTimer.text) - 1;
        GameTimer.text = curTime.ToString();
        if(curTime > 0) {
            Invoke("CountDown",1);
        }
        else {
            Time.timeScale = 0;
            gameEndPanel.SetActive(true);
            ScoreEndText.text = PlayerPrefs.GetString("score2");

        }
    }

    public void scoreInc() {
		score++;
		ScoreText.text = score + "";
      
      
    }

	public void scoreDec() {
		score--;
		ScoreText.text = score + "";
        
    }

	public void livesInc() {
		lives++;
		
	}

	public void livesDec() {
        
            lives--;
           
        LiveImage.sprite = hearts[lives];
            if(lives <= 0 ) {
                stop();
                gameEndPanel.SetActive(true);
            ScoreEndText.text = PlayerPrefs.GetString("score2");
            Time.timeScale = 0;
            
        }
	}
	
	// Update is called once per frame
	void Update () {
        PlayerPrefs.SetString("score2",ScoreText.text);
    }      


	public void stop() {
		run = false;
	}
}	
	

