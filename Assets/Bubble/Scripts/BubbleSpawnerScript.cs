﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleSpawnerScript : MonoBehaviour {


	public GameObject Bubble;
    public GameObject BubbleSpawnPos;
	public GameObject ScoreManager;
	private ScoreManger scoreManager;

    

    // Use this for initialization
    void Start () {
        scoreManager = GameObject.FindGameObjectWithTag("scoremanger").GetComponent <ScoreManger>() ;
		StartCoroutine (RandomBubbles ());

	}

	// Update is called once per frame
	void Update () {
      

	}

	

	void SpawnBubbles(){
		for (int i = 0; i < scoreManager.spawnCount; i++) {
			Vector3 pos = new Vector3 (Random .Range( BubbleSpawnPos .transform .position .x +30,BubbleSpawnPos.transform.position.x - 30),Random .Range ( BubbleSpawnPos.transform.position.y  + 40,BubbleSpawnPos.transform.position.y + 50 ),BubbleSpawnPos.transform.position.z );
			GameObject inst = Instantiate (Bubble, pos, Quaternion .identity );

			GameObject password = inst.transform.GetChild (0).gameObject;
			TextMesh pass = password.GetComponent<TextMesh> ();
			pass.text = randomPassword ();
	     //	scoreManager.oldCount++;
		}
	}

	private string[] myArray = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"1","2","3","4","5", "6", "7", "8", "9", "0",
		"@", "#", "$", "%", "^", "&", "*", "!", "/", "|", "/","<",">","?" };

	string randomPassword() {
		int size = Random.Range (6, 9);
        string pass = "";
        for (int i = 0; i < size; i++) {
			int offset = Random.Range (0, myArray.Length - 1);
			pass += myArray [offset];
		}
		return pass;
	}


	IEnumerator RandomBubbles(){

		while (true) {
			if (scoreManager.run) {
				//if (scoreManager.oldCount % 6 == 0 && scoreManager.oldCount > 0) {
				//	scoreManager.spawnCount++;
				//}
				////scoreManager.speed += 1f;
				////if (scoreManager.speed > 20) {
				////	scoreManager.speed = 20;
				////}
				//scoreManager.wait -= 0.5f;
				//if (scoreManager.wait < 1) {
				//	scoreManager.wait = 1;
				//}
				SpawnBubbles ();
                //yield return new WaitForSeconds (scoreManager.wait);
                yield return new WaitForSeconds (5);
            }
        }
	}



}
