﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBubbleURLS:MonoBehaviour {
    public GameObject Bubble;
    public GameObject BubbleSpawnPos;
    public GameObject ScoreManager;
    private ScoreManger scoreManager;
  public static int offset;
    Dictionary<int,string> URL = new Dictionary<int,string>();
    // Use this for initialization
    void Start() {
        scoreManager = GameObject.FindGameObjectWithTag("scoremanger").GetComponent<ScoreManger>();
        StartCoroutine(RandomBubbles());
        Dictionary<int,string> URL = new Dictionary<int,string>();

    }

    void SpawnBubbles() {
        for(int i = 0;i < scoreManager.spawnCount;i++) {
            Vector3 pos = new Vector3(Random.Range(BubbleSpawnPos.transform.position.x + 30,BubbleSpawnPos.transform.position.x - 30),Random.Range(BubbleSpawnPos.transform.position.y + 40,BubbleSpawnPos.transform.position.y + 50),BubbleSpawnPos.transform.position.z);
            GameObject inst = Instantiate(Bubble,pos,Quaternion.identity);

            GameObject URL = inst.transform.GetChild(0).gameObject;
            TextMesh url = URL.GetComponent<TextMesh>();
            url.text = RandomURLS();

        }
    }

    private string RandomURLS() {
        offset = Random.Range(1,URL.Keys.Count);
        if(!URL.ContainsKey(offset)) {

            URL.Add(1,"https://www.google.com");
            URL.Add(2,"https://www.amazon.com");
            URL.Add(3,"https://www.paypal.com");
            URL.Add(4,"https://www.facebook.com");
            URL.Add(5,"https://www.apple.com");
            URL.Add(6,"https://www.tripadvisor.com");
            URL.Add(7,"https://developers.google.com");
            URL.Add(8,"https://www.netflix.com");
            URL.Add(9,"wwwcnn.com");
            URL.Add(10,"www.amazone.com");
            URL.Add(11,"www.paypalsecure.com");
            URL.Add(12,"www.fbconnect.110mb.com");
            URL.Add(13,"http://www.mybonk.com");
            URL.Add(14,"http://www.mybank.com.scamsite.com");
            URL.Add(15,"developers.g00gle.com");
            URL.Add(16," www.hsbc-direct.com");
            URL.Add(17,"www.natwesti.com");
            URL.Add(18,"www.barclaya.net");
            URL.Add(19,"www.barclays-supports.com");
            URL.Add(20,"www.lloydstsbs.com");
            URL.Add(21,"netflix.om");
            URL.Add(22,"Micosoft.com");
            URL.Add(23,"Microsoft.info.com");
            URL.Add(24,"Microsoft-info.com");
            URL.Add(25,"Microsoft.com@no.how.com");

        }



        return URL[offset];



    }


    IEnumerator RandomBubbles() {

        while(true) {
            if(scoreManager.run) {

                SpawnBubbles();
           
                yield return new WaitForSeconds(5);
               
            }
        }
    }

    public bool CheckURL() {
       
        if(URL.ContainsKey(offset) && offset <= 8 ) {
       
            return true;
        }
     
        return false;

        
    }

  
}