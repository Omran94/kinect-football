﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleMove : MonoBehaviour  {


	public float speed=10;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.y <= -40) {
		
			Destroy(this.gameObject);
		}
		this.transform.Translate (Vector3.down *Time.deltaTime *speed,Space.World);
	}
   
}
